import 'package:flutter/material.dart';
import 'package:foodapp/auth.dart';
import 'package:foodapp/rubrica/client.dart';
import 'package:foodapp/track/stadi.dart';
import 'package:foodapp/rubrica/fornitori.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:foodapp/haccp/home.dart';
import 'package:foodapp/cronologia/home.dart';

class HomePage extends StatelessWidget {
  var email;

  HomePage({this.auth, this.onSignOut, Key id, this.email});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  FirebaseUser currentUser; 

  

  String _email() {
    if (currentUser != null) {
      return currentUser.displayName;
    } else {
      return "no current user";
    }
  }

  


  
 
  


  @override
  Widget build(BuildContext context) {

    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }

    }
    



    return new Scaffold(
      appBar: new AppBar(
        title: Text('AppFood'),
    
        actions: <Widget>[

          new RaisedButton(
             color: Colors.blue,
             
             onPressed: _signOut,
             //child: new Text('Logout', style: new TextStyle(fontSize: 17.0, color: Colors.white)),
             child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  
                  Padding(
                    padding: const EdgeInsets.only(right:8.0),
                    child: new Text('Logout',style: new TextStyle(fontSize: 17.0, color: Colors.white)),
                  ),
                  new Icon(Icons.exit_to_app),
                ],
              ),
            
          )
        ],
      ),
      
      body: StaggeredGridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 12.0,
                mainAxisSpacing: 12.0,
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                children: <Widget>[
                  _buildTile(
                   
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              
                              Text('Tracciabilità', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                            ],
                          ),
                          Material
                          (
                            
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(24.0),
                            child: Center
                            (
                              child: Padding
                              (
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.timeline, color: Colors.white, size: 30.0),
                                
                              )
                            )
                          )
                        ]
                      ),
                    ),
                    onTap: (){
                      Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new stadi()),
                    );}
                  ),
                   _buildTile(
                   
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                          (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              
                              Text('Haccp', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                            ],
                          ),
                          Material
                          (
                            
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(24.0),
                            child: Center
                            (
                              child: Padding
                              (
                                padding: const EdgeInsets.all(16.0),
                                child: Icon(Icons.timeline, color: Colors.white, size: 30.0),
                                
                              )
                            )
                          )
                        ]
                      ),
                    ),
                    onTap: (){
                      Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new homehaccp()),
                    );}
                    
                  ),
                  _buildTile(
                    Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: Column
                      (
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Material
                          (
                            color: Colors.teal,
                            shape: CircleBorder(),
                            child: Padding
                            (
                              padding: const EdgeInsets.all(16.0),
                              child: Icon(Icons.contacts, color: Colors.white, size: 30.0),
                            )
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text('Clienti', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 24.0)),
                         
                        ]
                      ),
                    ),
                    onTap: (){
                      Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new ClientPage()),
                    );
                    }
                  ),
                  _buildTile(
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Column
                      (
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Material
                          (
                            color: Colors.amber,
                            shape: CircleBorder(),
                            child: Padding
                            (
                              padding: EdgeInsets.all(16.0),
                              child: Icon(Icons.perm_identity, color: Colors.white, size: 30.0),
                            )
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text('Fornitori', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 24.0)),
                          
                        ]
                      ),
                    ),
                    onTap: (){
                      Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new fornitori()),
                    );
                    }
                  ),
                  _buildTile(
                    Padding
                    (
                      padding: const EdgeInsets.all(24.0),
                      child: Column
                      (
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Material
                          (
                            color: Colors.red,
                            shape: CircleBorder(),
                            child: Padding
                            (
                              padding: EdgeInsets.all(16.0),
                              child: Icon(Icons.store, color: Colors.white, size: 30.0),
                            )
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text('Cronologia', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 24.0)),
                          
                        ]
                      ),
                    ),
                    onTap: (){
                     Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new homecrono()),
                    );
                    }
                  ),
                  
                 
                ],
                staggeredTiles: [
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(2, 110.0),
                  StaggeredTile.extent(1, 180.0),
                  StaggeredTile.extent(1, 180.0),
                  StaggeredTile.extent(2, 180.0),
                  
                ],
              )
            );
    
  }
   Widget _buildTile(Widget child, {Function() onTap}) {
            return Material(
              elevation: 14.0,
              borderRadius: BorderRadius.circular(12.0),
              shadowColor: Color(0x802196F3),
              child: InkWell
              (
                // Do onTap() if it isn't null, otherwise do print()
                onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
                child: child
              )
            );
          }

}