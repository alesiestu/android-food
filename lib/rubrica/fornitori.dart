import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/rubrica/editfornitori.dart';
import 'package:foodapp/rubrica/nuovofornitore.dart';


class fornitori extends StatefulWidget {
  fornitori({this.email});
  final String email;
  @override
  _fornitoriState createState() => new _fornitoriState();
}

class _fornitoriState extends State<fornitori> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
     
     floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.add),
       backgroundColor: Colors.blue,
       onPressed: () {
            Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => new Nuovofornitore()));},
            
     ),
     floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
     bottomNavigationBar: new BottomAppBar(
       elevation: 20,
       color: Colors.grey,
       child: ButtonBar(
         children: <Widget>[],
       ),
     ),
     
     
     
     
     body: 
     new Stack(
     children: <Widget>[  
     
     new Padding(
            padding: const EdgeInsets.only(top:160),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("fornitori")
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
       return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),


     Container(
       height: 200.0,
       width: double.infinity,
       decoration: BoxDecoration(
         boxShadow: [
           new BoxShadow(
             color: Colors.black,
             blurRadius: 8.0
           )
         ],
         color: Colors.blue
       ),

        child: new Padding(
       padding: const EdgeInsets.all(18.0),
       child: Row(
         mainAxisAlignment: MainAxisAlignment.center,
         children: <Widget>[
          
          new Text("Lista fornitori",style: new TextStyle(color: Colors.white, fontSize: 30, letterSpacing: 2), )
           
         ],
       ),
     ),
     ),
     ]
    )
    );
  }
}

class listaclienti extends StatelessWidget {
  listaclienti({this.document});
  final List<DocumentSnapshot> document;
  
  @override
  Widget build(BuildContext context){
    
    
    return new ListView.builder(
      itemCount:document.length,
      itemBuilder:(BuildContext context,  int i){
     // String title= document[i].data['titolo'].toString();
      String nome= document[i].data['nome'].toString();
      String indirizzo= document[i].data['indirizzo'].toString();
      String iva= document[i].data['iva'].toString();
      String telefono= document[i].data['telefono'].toString();
      
      return new Dismissible(
        key: new Key(document[i].documentID),
        onDismissed: (direction){
          Firestore.instance.runTransaction((transaction)async{
            DocumentSnapshot snapshot=
            await transaction.get(document[i].reference);
            await transaction.delete(snapshot.reference);
          });

          Scaffold.of(context).showSnackBar(
            new SnackBar(content: new Text("Fornitore cancellato"),)
          );
        },

        child: new Padding(
         padding: const EdgeInsets.only(left: 16, top: 25, right: 16, bottom: 8),
            
          
          child: Row(
            children: <Widget>[
              new Expanded(
                            child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      Padding(
                        padding: const EdgeInsets.only(bottom:8.0),
                        child: Text(nome, style: new TextStyle(fontSize: 20.0, letterSpacing: 1.0),),
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right:16.0),
                            child: Icon(Icons.location_city, color: Colors.blue,),
                          ),
                          Text(indirizzo, style: new TextStyle(fontSize: 18.0, letterSpacing: 1.0),),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right:16.0),
                            child: Icon(Icons.info, color: Colors.blue,),
                          ),
                          Text(iva, style: new TextStyle(fontSize: 18.0, letterSpacing: 1.0),),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(right:16.0),
                            child: Icon(Icons.phone, color: Colors.blue,),
                          ),
                          Text(telefono, style: new TextStyle(fontSize: 18.0, letterSpacing: 1.0),),
                        ],
                      ),

                    ],

        ),
                ),
              ),

             new IconButton(
               icon: Icon(Icons.edit, color:Colors.black),
               onPressed: (){
                 Navigator.of(context).push(new MaterialPageRoute(
                   builder: (BuildContext context)=> new editfornitori(
                     nome: nome,
                     indirizzo: indirizzo,
                     iva: iva,
                     telefono: telefono,
                     index: document[i].reference,

                   )
                 ));
               },
             )
            ],
          ),
        
        ),
      );
      },

    );
  }
  _fornitoriState createState() => new _fornitoriState();
}