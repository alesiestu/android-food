import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:foodapp/track/ingressoa3.dart';
import 'package:flutter/services.dart';


class ingressoa2 extends StatefulWidget{
  ingressoa2({this.nome, this.iva, this.index });
  final String nome;
  final String iva;
  final index;

  @override
  _ingressoa2State createState() => new _ingressoa2State(); 
}

class _ingressoa2State extends State<ingressoa2>{

  String nome;
  String iva;
  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  String _testodata2 = '';
  File image;
  String filename='';
  String indirizzofirebase;
  String pathimg='';
 // String get immagine=> '$iva'+'$_testodata2' ;

  Future _getimage() async{
    var selectedImage= await ImagePicker.pickImage(source: ImageSource.camera,maxHeight: 400,maxWidth: 400);
    setState(() {
          
          image=selectedImage;

        //  filename=immagine;
          filename=basename(image.path);
          pathimg=image.path.toString();

          
        });
  }

  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      nome=widget.nome;
      iva=widget.iva;
      _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
      SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
    }

  @override
    Widget build(BuildContext context) {
        
    void pagina(){
    
    Navigator.of(context).push(new MaterialPageRoute(
                   builder: (BuildContext context)=> new ingressoa3(
                     nome: nome,
                     iva: iva,
                     urlimmagine: indirizzofirebase,
                     pathimmagine:pathimg,
                     data: _testodata,
                     filename: filename,
                    
                     
                     
                     
                   )
      )
    );}
   return new Scaffold(

    
    appBar: 
      new AppBar(
        title: Text('$nome'),
      ),

      floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.photo_camera),
       backgroundColor: Colors.blue,
       onPressed: _getimage,


       
            
            
     ),
     floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
     bottomNavigationBar: new BottomAppBar(
       elevation: 20,
       color: Colors.grey,
       child: ButtonBar(
         children: <Widget>[],
       ),
     ),
    
    
    body: 
       Column(
       children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Expanded( child: Text("Data ingresso:", style:new TextStyle(fontSize:22.0,color: Colors.black ))),
                         new FlatButton(
                          onPressed: ()=> _selezionadata(context),
                           child:Text(_testodata, style:new TextStyle(fontSize:22.0,color: Colors.black )))
                      ],
                    ),
                  )
                ],
              ),

            ),
          ),

          Padding(
            padding: const EdgeInsets.all(16.0),
            child: 
            new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              
            
            
            
            image==null?Text("Prima di procedere con l'inserimento bisogna scattare una foto al lotto", style: new TextStyle(fontSize: 20),textAlign: TextAlign.center,):(

               
               Column(children: <Widget>[
               
               
               Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                 children: <Widget>[
                   
                   new ListTile(
                    
                    title: Column(
                      children: <Widget>[
                        Text('Foto lotto :',style: new TextStyle(fontSize: 20),),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.file(image, width: 300, height: 300,),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top:12.0),
                          child: RaisedButton(
                            color: Colors.yellow,
                
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                Column(
                                  
                                  children: <Widget>[
                                  
                                  Text('Etichetta lotto', style: TextStyle(fontWeight:FontWeight.bold), textAlign:TextAlign.center,),
                                
                                ],)
                    
                    
                                ],
                              ),
                              
                
                                  onPressed:() {
                                  uploadfirebase().whenComplete(pagina);
                                  //pagina();
                                  //indirizzofirebase=uploadfirebase().toString();
                                  
                          
                    
                                  },
                                          
                                        ),
                        )
                                    ],
                                  )
                                  
                                  
                                ),
                    
                    
                   
                                  ],
                                ),
                                
                                
                                
                                  

                                ],)

                                ),
                              
            


            ],
            ),
          ),
         
         
         
          
 
        
       ],
       


     ),

     

    
    
    
    );


    
    
    
    
    }

    



Future<String> uploadfirebase() async{
  StorageReference ref= FirebaseStorage.instance.ref().child(filename);
  StorageUploadTask uploadTask=ref.putFile(image);
  var downurl = await (await uploadTask.onComplete).ref.getDownloadURL();
  var url=downurl.toString();
  print(url);
  
 
  
  setState(() {
        url=indirizzofirebase;
        
     
          });
  
  return url;
}
}

