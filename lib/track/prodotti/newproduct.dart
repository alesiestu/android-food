import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/prodotti/home.dart';



class newproduct extends StatefulWidget{

   @override
  _product createState() => new _product();
}



class _product extends State<newproduct>{
   String nome='';
   String quantita='';

    void _addData(){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('prodotti');
       await reference.add({
         "nome" : nome,
         "quantita": quantita,
         "dataproduzione":_testodata,
         

         

       });
       Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new homeprodotti(),
                        ),
                        ModalRoute.withName('/'));
       
     });

    
     

  }
   
   //data
   String _testodata = '';
   DateTime _datacorrente= new DateTime.now();
    Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

  //data


  @override
    void initState() {
      // TODO: implement initState
      super.initState();
       _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
    }



  @override
  Widget build (BuildContext context){

    return Scaffold(
      appBar: new AppBar(
        title: Text('Nuovo prodotto'),
        actions: <Widget>[
      
            ],
      ), 
       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.save),
       backgroundColor: Colors.blue,
       onPressed:(){
       _addData();
        
      

       },
            
            
             ),
            floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
            bottomNavigationBar: new BottomAppBar(
              elevation: 20,
              color: Colors.grey,
              child: ButtonBar(
                children: <Widget>[],
              ),
            ),


      body: ListView(
         shrinkWrap: true,
              padding: EdgeInsets.all(15.0),
              children: <Widget>[

       
      Padding(
        padding: const EdgeInsets.only(top:8.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top:8.0),
              child: 
              
              Card(

                child: 
                
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                 
                  
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Dettagli prodotto', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                  ),

                  

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Nome:',style: TextStyle(fontSize: 18)),
                  ),
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: TextField(
                     
                      onChanged: (String str){
                        setState(() {
                          nome=str; 
                                
                                          });
                        },
                        decoration: new InputDecoration(
                
                
                      
                

                              ),
                            ),
                   ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Quatità:',style: TextStyle(fontSize: 18)),
                            ),
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: TextField(
                      keyboardType: TextInputType.number,
                      onChanged: (String str){
                        setState(() {
                          quantita=str; 
                                
                                          });
                        },
                        decoration: new InputDecoration(
                
                
                         
                

                              ),
                            ),
                   ),
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: new Row(
                        children: <Widget>[
                          new Expanded( child: Text("Data ingresso:", style:new TextStyle(fontSize:22.0,color: Colors.black ))),
                           new FlatButton(
                            onPressed: ()=> _selezionadata(context),
                             child:Text(_testodata, style:new TextStyle(fontSize:22.0,color: Colors.black )))
                        ],
                      ),
                   ),
                                  
                                ],
                              ),
              ),
            ),
            
            
          
            
          ],
        ),
      )

      ])


    );

  }
}