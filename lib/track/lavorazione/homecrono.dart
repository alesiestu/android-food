import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/lavorazione/storico.dart';


class homecrono extends StatefulWidget{
  homecrono({this.testodata});
  final String testodata;
  
  
  @override
  _homecronoState createState() => new _homecronoState(); 
}

class _homecronoState extends State<homecrono>{
  
  String testodata;

  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      
      testodata=widget.testodata;
    }
 

    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Seleziona fornitore'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("fornitori")
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,testodata: testodata,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatefulWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path,this.testodata});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;
  final String testodata;
   
   @override
  listafornitoristate createState() => new listafornitoristate(); 

}


class listafornitoristate extends State<listaclienti>{
  List<DocumentSnapshot> document;
  String nome;
  String iva;
  String path;
  String dataselezionata;
 
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      nome=widget.nome;
      iva=widget.iva;
      path=widget.path;
      document=widget.document;
      dataselezionata=widget.testodata;


    }

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String nome= document[i].data['nome'].toString();
            String indirizzo= document[i].data['indirizzo'].toString();
            String iva= document[i].data['iva'].toString();
            String telefono= document[i].data['telefono'].toString();

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: new Text(
                   nome
                ,style: TextStyle(fontSize: 20 ),
                ),
                subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:8.0),
                  child: new Text("Partita iva : $iva ",style: TextStyle(fontSize: 20),),
                ),
               
                ])
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.add_box),
                        Text('Seleziona',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 

                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new homestorico(
                            fornitore: nome,
                            data: dataselezionata,
                            
                            
                          
                          )
                        )
                        );


                      },
                    ),
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _homecronoState createState() => new _homecronoState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
