import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:foodapp/track/lavorazione/lavorazione.dart';



class dettagli extends StatefulWidget{
  dettagli({this.path});
  final String path;
  
  @override
  _dettagli createState() => new _dettagli();

}


class _dettagli extends State<dettagli>{
 String immagine;

 List<String> selectedCities = [];
 DateTime _datacorrente= new DateTime.now();
 


 


@override
  void initState() {
    // TODO: implement initState
    super.initState();
   immagine=widget.path;

  }
  
  
   

  


@override
    Widget build(BuildContext context) {
   
    return Scaffold(
      


      appBar: new AppBar(
        title: Text('Scheda prodotto'),
    
        actions: <Widget>[


          
        ],
      ),
      

       
    

    body: 
    
    new Stack(
      
     
     children: 
     
     <Widget>[ 
      
  
     
     new Padding(
            padding: const EdgeInsets.only(top:8),
            child:
     
     StreamBuilder(

       stream: Firestore.instance
       .collection("ingresso").where('pathimmagine', isEqualTo: immagine)
       .snapshots(),

      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
           return new Container( child: Center(
           child: CircularProgressIndicator()
        ),

        );
      
      
      
      
      
      
      return new dettaglilavorazioni(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )
          



    );
    }


}



class dettaglilavorazioni extends StatelessWidget {

  


  
  
  dettaglilavorazioni({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  
  final nome;
  final iva;
  final path;
  List<DocumentSnapshot> document2;
 

  

  @override
  Widget build(BuildContext context){
    



    
      return ListView.builder(
            itemCount: document.length,
          
            itemBuilder: (BuildContext context, int i) {
             
             
          
            
         
            String data= document[i].data['data'].toString();
            String etichetta= document[i].data['etichetta'].toString();
            String nomefornitore= document[i].data['nomefornitore'].toString();
            String pathimmagine=document[i].data['pathimmagine'];
            String ivafornitore=document[i].data['ivafornitore'];

           

            
             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: Column(
                  children: <Widget>[
                   Column(
                     mainAxisAlignment: MainAxisAlignment.center,
                     crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        
                         Padding(
                           padding: const EdgeInsets.all(8.0),
                           child: Text(etichetta, style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                         ),
                         Image.network(pathimmagine,width: 300,height: 300),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:16.0),
                      child: Column(children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Data ingresso prodotto:',style: TextStyle(fontSize: 16),),
                              ),
                              Text(data,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Nome fornitore',style: TextStyle(fontSize: 18),),
                              ),
                              Text(nomefornitore,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Iva fornitore',style: TextStyle(fontSize: 18),),
                              ),
                              Text(ivafornitore,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                            ],
                          ),
                        ),
                        
                      ],),
                    )
                     

                   

                  ],
                ),
                
                
                
              ),
               new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                       
                        Text('Torna alla lista',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 

                        Navigator.pop(context);


                      },
                    ),
                    
                  ],
                ),
              ),
              
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _dettagli createState() => new _dettagli();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
