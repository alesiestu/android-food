import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:async';
import 'package:foodapp/track/lavorazione/test2.dart';
import 'dart:math';

import 'package:foodapp/track/lavorazione/util/lavorazionemodel.dart';
import 'package:foodapp/track/lavorazione/service/firestoredataservice.dart';

class lavorazione extends StatefulWidget{
  lavorazione({this.nomeprodotto});
  final String nomeprodotto;
  
  @override
  _lavorazione createState() => new _lavorazione();

}


class _lavorazione extends State<lavorazione>{
String nomeprodotto;
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    nomeprodotto=widget.nomeprodotto;


  }

  


@override
    Widget build(BuildContext context) {
    return Scaffold(
      


      appBar: new AppBar(
        title: Row(
          children: <Widget>[
            Text('Seleziona materie prime')
          ],
        ),

    
        actions: <Widget>[


          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
    
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("ingresso")
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new ListaProdotti(document: snapshot.data.documents,nomeprodotto:nomeprodotto);
      },

      
      
     ),
    
      
     ]
    )




    );
    }


}
 class Member {
  String firstname;
  String lastname;
  String username;
  Member(this.firstname, this.lastname, this.username);


 }

  
class ListaProdotti extends StatefulWidget{
  final String nomeprodotto;
  
  
  
  ListaProdotti({this.document,this.nomeprodotto});
  final List<DocumentSnapshot> document;
  @override
  _ListaProdottiState createState() => new _ListaProdottiState();
  
  
}

class _ListaProdottiState extends State<ListaProdotti>{
  DateTime _datacorrente= new DateTime.now();
  String nomeprodotto;

   FirebaseFirestoreService db = new FirebaseFirestoreService();
  

  List<DocumentSnapshot> document;
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      document=widget.document;
      nomeprodotto=widget.nomeprodotto;
    }

  void updateclienti(){  //in caso di eliminazione
  
    Firestore.instance.runTransaction((Transaction transaction)async {
      DocumentSnapshot snapshot=
      await transaction.get(selectProduct[0]);
      await transaction.update(snapshot.reference, {
        "abilitato": "test concluso",
      

      });
    });
    Navigator.pop(context);

  }


  void _addData(){
      List myList = [selectProduct, fornitori,immagini];  
       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('lavorazione');
       await reference.add({
         "data" : _datacorrente,
        // "codiceingresso": myList[0],
        // "fornitore": myList[1],
         "materieprime": selectProduct,
         "nomeprodotto":nomeprodotto
       });
     });    
     Navigator.pop(context);
  }

  void adddatacheck(String etichetta,String pathimmagine, String fornitore, String iva, String quantita, String note,String data, var materia ){
       db.createNote(etichetta, pathimmagine);
       

       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('lavorazione');
       await reference.add({
        
        "data" : _datacorrente, 
        "materieprime": etichetta,
        "nomeprodotto":nomeprodotto,
        "pathimmagine":pathimmagine,
        "nomefornitore":fornitore,
        "ivafornitore":iva,
        "quantita":quantita,
        "note":note,
        "dataingresso":data,
        "codicemateria":materia,
        "inserito":"0"


       });
     });    
     
  }
  void numero() {
  var rng = new Random();
  
  var next = rng.nextDouble() * 1000000;
  while (next < 100000) {
    next *= 10;
  }
  print(next.toInt());
  setState(() {
      var codice=next.toInt();
    });
}
  

  List selectProduct = List();
  List etichetteProduct = List();
  List fornitori = List();
  List immagini=List();
  var lavori=<Map>[];
  String eti='nome etichetta ';
  String fornitoda=' da ';
  final members = new List<Member>();

  void delete(codice){
    final WriteBatch _batch = Firestore.instance.batch();
    var laRef = Firestore.instance.collection("cities").document();
    
    _batch.delete(laRef);
    
    


    
   Firestore.instance.collection('lavorazione').document().updateData({'Desc': FieldValue.delete()}).whenComplete((){
   print('Field Deleted');
});
  }
 
   void _onCategorySelected(bool selected, category_id, nome, fornitore,pathimmagine, iva,quantita,note,data) {
    if (selected == true) {
      //metodoinserimento nel DB
      adddatacheck(nome,pathimmagine,fornitore,iva,quantita,note,data,category_id);
      setState(() {
        selectProduct.add(category_id);
        etichetteProduct.add(nome);
        fornitori.add(fornitore);
        immagini.add(pathimmagine);
        

      });
    } else {
      //metodo cancellazione proodotti
      delete(category_id);
      setState(() {
        selectProduct.remove(category_id);
        etichetteProduct.remove(nome);
      });
    }
  }
  
  
   @override
   Widget build(BuildContext context){


     return Scaffold(

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.save),
       backgroundColor: Colors.blue,
       onPressed:(){
        showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Procedere con il salvataggio?'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Cancella',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Si',style: TextStyle(fontSize: 14)),
                onPressed: (){

                  _addData();
                  //test();
                  
                },
              )
            ],
          )
        );
      

       },
            
            
     ),
     
   


 
            
            
     



  body:
   ListView.builder(
    
    padding: const EdgeInsets.all(4.0),
    
    itemCount: document.length,
                      
    
    
    
    
     itemBuilder: (BuildContext context, int i) {
      
       
       String urlimmagine=document[i].data['pathimmagine'].toString();
       String etichetta= document[i].data['etichetta'].toString();
       String nomefornitore= document[i].data['nomefornitore'].toString();
       String ivafornitore= document[i].data['ivafornitore'].toString();
       String quantita= document[i].data['quantita'].toString();
       String note= document[i].data['note'].toString();
       String data= document[i].data['data'].toString();
       String pathimmagine=document[i].data['pathimmagine'].toString();
       var codice= Key(document[i].documentID);
       String value;
       bool checked;

        
      return new Card (
        child:
             CheckboxListTile(
              value: selectProduct.contains(document[i].reference),   //_selecteCategorys.contains(_categories['responseBody'][001]['category_id']),
              onChanged: (bool selected) {
                _onCategorySelected(selected,
                    document[i].reference,etichetta,nomefornitore,pathimmagine,ivafornitore,quantita,note,data);
              },
              title: 
              
                Row(
                  children: <Widget>[
                    Column(children: <Widget>[
                    Image.network(urlimmagine,width: 100,height: 130),
                    
                    

              ],),
              Padding(
                padding: const EdgeInsets.only(left:16.0),
                child: Column(
                  
                       
                        children: <Widget>[
                          Text('Nome etichetta: '),
                          Text(etichetta),
                          Padding(
                            padding: const EdgeInsets.only(top:4.0),
                            child: Text('Data fornitura '),
                          ),
                          Text(data),
                          
                        ],
                      ),
              ),
                  ],
                ) 
            ),
        );
       
       
       
       



       
      








     }




  ),
     );
  
  
  }

  _lavorazione createState() => new _lavorazione();
  
  

}




