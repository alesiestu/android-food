import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:async';
import 'package:foodapp/track/lavorazione/test2.dart';
import 'dart:math';
import 'package:foodapp/track/stadi.dart';

import 'package:foodapp/track/lavorazione/util/lavorazionemodel.dart';
import 'package:foodapp/track/lavorazione/service/firestoredataservice.dart';

import 'package:flutter/cupertino.dart';
import 'package:foodapp/track/lavorazione/chiusura.dart';



class lavorazione extends StatefulWidget{
  lavorazione({this.nomeprodotto,this.codicesessione});
  final String nomeprodotto;
  final int codicesessione;

  
  @override
  _lavorazione createState() => new _lavorazione();
}

class _lavorazione extends State<lavorazione>{
var codicesessione;
String nomeprodotto;

String quantita;
String misura='';
int _radioValue = 0;

List<DocumentSnapshot> document1;
List<DocumentSnapshot> document2;
  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';

Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

@override
  void initState() {
    super.initState();
    nomeprodotto=widget.nomeprodotto;
    codicesessione=widget.codicesessione;
    _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
  }

  void chiudi(){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('prodottifiniti');
       await reference.add({
        "dataproduzione":_testodata,
        "codicesessione" : codicesessione, 
        "prodotto": nomeprodotto,
        "quantita":quantita,
        "misura":misura
       });
     });
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          misura = "pz";
          break;
        case 1:
          misura = "lt";
          break;
        case 2:
          misura = "kg";
          break;
      }
    });
  }

   Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Annullare inserimento?'),
        content: new Text('I dati non saranno salvati'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('NO'),
          ),
          new FlatButton(
            onPressed: () {

               Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => new stadi(),
                          ),
                          ModalRoute.withName('/'));
            },
            child: new Text('SI'),
          ),
        ],
      ),
    ) ?? false;
  }
  


@override
    Widget build(BuildContext context) {
    return  WillPopScope(
       onWillPop: _onWillPop,
          child: Scaffold(
        appBar: new AppBar(
          title: Row(
            children: <Widget>[
              Text('Prodotto: $nomeprodotto')
            ],
          ),
          actions: <Widget>[
                
                IconButton(
                
                icon: Icon(Icons.check),
                onPressed: () { //devo aggiungere la quantità
                
                  Navigator.of(context).push(new MaterialPageRoute(
                                  builder: (BuildContext context)=> new chiusura(
                                   nomeprodotto: nomeprodotto, 
                                   codicesessione: codicesessione,
                                  )
                                )
                                );
                 
                },
              ),
          ],
        ),
       
       body:   
       
       new Stack(
       children: <Widget>[ 
    
       StreamBuilder(
         stream: Firestore.instance
         .collection("ingresso").where("abilitato", isEqualTo: '0')
         .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
          if(!snapshot.hasData)
          return new Container( 
            child: Center(
            child: CircularProgressIndicator()
          ),
          );
         document1=snapshot.data.documents;
         return StreamBuilder(
         stream: Firestore.instance
         .collection("lavorazione").where("codicesessione", isEqualTo: codicesessione)
         .snapshots(), 
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot1 ){
        if(!snapshot1.hasData)
        return new Container( child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('Download dati dal server ', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            CircularProgressIndicator(),
          ],
        )
          ),
          );
        List<DocumentSnapshot> document2=snapshot1.data.documents;
        return new ListaProdotti(document: document1,document2:document2,codicesessione:codicesessione,nomeprodotto: nomeprodotto,);
        },   
       );
        //return new Nulla();
        }, 
       ),
       
       ]
      )
      ),
    );
    }
}



  
class ListaProdotti extends StatefulWidget{
  final String nomeprodotto;
  final codicesessione;
  ListaProdotti({this.document,this.nomeprodotto,this.document2,this.codicesessione});
  final List<DocumentSnapshot> document; //materie prime
  final List<DocumentSnapshot> document2;
  @override
  _ListaProdottiState createState() => new _ListaProdottiState();  
}


class _ListaProdottiState extends State<ListaProdotti>{
  //DateTime _datacorrente= new DateTime.now();
  String nomeprodotto;
  var codicesesione;
  String controllo="";
  bool pressed = true;

  FirebaseFirestoreService db = new FirebaseFirestoreService();

  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  var sessione;

  

  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }
  
  List<DocumentSnapshot> document;
  List<DocumentSnapshot> document2;
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      document=widget.document;  //materie prime
      nomeprodotto=widget.nomeprodotto;
      document2=widget.document2;
      codicesesione=widget.codicesessione;
      _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
       List<String> countries = <String>['Belgium','France','Italy','Germany','Spain','Portugal'];
    }
  
    reload(){
    Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new lavorazione(
                          
                           codicesessione: codicesesione,
                           nomeprodotto: nomeprodotto,
                          )
                        )
                        );
  }

  void delete(elemento){
    Firestore.instance.runTransaction((transaction)async{
            DocumentSnapshot snapshot=
            await transaction.get(elemento);
            await transaction.delete(snapshot.reference);
          }).whenComplete(
            conferma
     
          );
    
  }


  void adddatacheck(String etichetta,String pathimmagine, String fornitore, String iva, String quantita, String note,String data ){
      
       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('lavorazione');
       await reference.add({
        
        "data" : _testodata, 
        "materieprime": etichetta,
        "nomeprodotto":nomeprodotto,
        "pathimmagine":pathimmagine,
        "nomefornitore":fornitore,
        "ivafornitore":iva,
        "quantita":quantita,
        "note":note,
        "dataingresso":data,
        "codicesessione":codicesesione
       });
     });

     
     
         
  }

  void numero() {
  var rng = new Random();
  
  var next = rng.nextDouble() * 1000000;
  while (next < 100000) {
    next *= 10;
  }
  print(next.toInt());
  setState(() {
      var codicesessione=next.toInt();
    });
}
void _showDialog(elemento) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Cancellare selezione?"),
            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("No"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Si"),
              onPressed: () {
                delete(elemento);
                
              },
            ),
          ],
        );
      },
    );
}

conferma() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Rimozione effettuata"),
            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new FlatButton(
                  child: new Text("TORNA ALLA LISTA"),
                  onPressed: () {
                   reload();
                  },
                ),
              ],
            ),
           
          ],
        );
      },
    );
}
 var isSelected = false;
 var mycolor=Colors.white;
 void toggleSelection() {
    setState(() {
      if (isSelected) {
        mycolor=Colors.white;
        isSelected = false;
      } else {
        mycolor=Colors.grey[300];
        isSelected = true;
      }
    });
  }

  _showDialogexample(document) async{
    await showDialog<String>(
      context: context,
      builder: (BuildContext context){
        return new CupertinoAlertDialog(
          title: Padding(
            padding: const EdgeInsets.all(8.0),
            child: new Text('Seleziona la materia prima'),
          ),
          actions: <Widget>[
            new CupertinoDialogAction(
              isDestructiveAction: true,
              onPressed: (){Navigator.of(context).pop('Indietro');},
              child: new Text('Indietro'),
            ),
            new CupertinoDialogAction(
              isDestructiveAction: true,
              
              onPressed: (){
                
             Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new lavorazione(
                          
                           codicesessione: codicesesione,
                           nomeprodotto: nomeprodotto,
                          )
                        )
                        );
              
              
              
              },
              
              child: new Text('Conferma'),
            ),
          ],
          content: new SingleChildScrollView(
            child: new Material(
              child: new MyDialogContent(document: document, prodotto: nomeprodotto, sessione: codicesesione),
            ),
          ),
        );
      },
      barrierDismissible: false,
    );
  }

  
   @override
   Widget build(BuildContext context){


     return Scaffold(

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.add_box),
       backgroundColor: Colors.green,
       onPressed:(){
        _showDialogexample(document);
      

       },
            
            
     ),
     
   


 
            
            
     



  body:
  
   
   ListView.builder(
    
    padding: const EdgeInsets.all(4.0),
    
    itemCount: document2.length,
                      
    
    
    
    
     itemBuilder: (BuildContext context, int i) {
      
       
       String urlimmagine=document2[i].data['pathimmagine'].toString();
       String etichetta= document2[i].data['materieprime'].toString();
       String nomefornitore= document2[i].data['nomefornitore'].toString();
       String ivafornitore= document2[i].data['ivafornitore'].toString();
       String quantita= document2[i].data['quantita'].toString();
       String note= document2[i].data['note'].toString();
       String data= document2[i].data['data'].toString();
       String dataingresso= document2[i].data['dataingresso'].toString();
       String pathimmagine=document2[i].data['pathimmagine'].toString();
       
      
      return Column(
        children: <Widget>[
          new Card (
            child:
            
                 
                 Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: ListTile( 
                    subtitle: Column(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Riepilogo fonitura: Fornito da $nomefornitore il giorno $dataingresso'),
                      )
                    ],),  
                    title: 
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                      padding: const EdgeInsets.all(1.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(etichetta,style: new TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                                ),
                              ],
                            ),
                    ),
                    Column(children: <Widget>[
                          Image.network(urlimmagine,width: 200,height: 150),
                    ],),
                        ],
                      ), 
                onLongPress: (){ _showDialog(document2[i].reference);

                },),
                 ),
            ),
        ],
      );
     }
  ),
     );
  }
  _lavorazione createState() => new _lavorazione();
}



class MyDialogContent extends StatefulWidget {
 
 
  MyDialogContent({
   this.document,
   this.sessione,
   this.prodotto
  });
  var sessione;
  var prodotto;
  
  
  List<DocumentSnapshot> document;  //materie prime
  //final List<String> countries;

  @override
  _MyDialogContentState createState() => new _MyDialogContentState();
}

class _MyDialogContentState extends State<MyDialogContent> {
  int _selectedIndex = 0;

  DateTime _datacorrente= new DateTime.now();
  String _testodata = '';
  var sessione;
  var prodotto;
  bool pressed = true;

  Map<int, bool> itemsSelectedValue = Map();


   List<String> _selectedTile = List();
    
   


  
  
  void adddatacheck(String etichetta,String pathimmagine, String fornitore, String iva, String quantita, String note,String data,var iddocumento ){
      
       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('lavorazione');
       await reference.add({
        
        "data" : _testodata, 
        "materieprime": etichetta,
        "nomeprodotto":prodotto,
        "pathimmagine":pathimmagine,
        "nomefornitore":fornitore,
        "ivafornitore":iva,
        "quantita":quantita,
        "note":note,
        "dataingresso":data,
        "codicesessione":sessione,
        "iddocumento":iddocumento
       });
     });

     
     
         
  }

 var isSelected = false;
 var mycolor=Colors.white;
 
 void toggleSelection() {
    
    setState(() {
      if (isSelected) {
        mycolor=Colors.white;
        isSelected = false;
      } else {
        mycolor=Colors.grey[300];
        isSelected = true;
      }
    });
  
  } 

   

  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }

     List selectProduct = List();
     void _onCategorySelected(bool selected, codice) {
    if (selected == true) {
      setState(() {
        selectProduct.add(codice);
       
        

      });
    } else {
      setState(() {
        selectProduct.remove(codice);
        
      });
    }
  }

  void deletesingle(elemento){
    
    var ref_value;
    
    Firestore.instance
    .collection('lavorazione')
    .where('iddocumento', isEqualTo: elemento)
    .limit(1)
    .getDocuments()
    .then((doc) {
    ref_value = doc.documents[0].reference;
    });
    
    
    Firestore.instance.runTransaction((transaction)async{
            DocumentSnapshot snapshot=
            await transaction.get(ref_value);
            await transaction.delete(snapshot.reference);
    });
    
  }

  void reload(){
    Navigator.of(context).push(new MaterialPageRoute(
                                  builder: (BuildContext context)=> new lavorazione(
                                   nomeprodotto: prodotto, 
                                   codicesessione: sessione,
                                  )
                                )
                                );
  }


  List<DocumentSnapshot> document;  
  Color color;
  bool _color;
  @override
  void initState(){
    super.initState();
     _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";

    document=widget.document;
    sessione=widget.sessione;
    prodotto=widget.prodotto;
    color = Colors.white;
     _color=true;
  }

  _getContent(){
    if (widget.document.length == 0){
      return new Container();
    }

    return new Column(
      children: <Widget>[

        Container(
                   height: 500.0, // Change as per your requirement
                   width: 500.0, // Change as per your requirement
                   child: new ListView.builder(
                   padding: const EdgeInsets.all(4.0),
                   itemCount: document.length,
                   itemBuilder: (BuildContext context, int i) {
                   String urlimmagine=document[i].data['pathimmagine'].toString();
                   String etichetta= document[i].data['etichetta'].toString();
                   String nomefornitore= document[i].data['nomefornitore'].toString();
                   String ivafornitore= document[i].data['ivafornitore'].toString();
                   String quantita= document[i].data['quantita'].toString();
                   String note= document[i].data['note'].toString();
                   String data= document[i].data['data'].toString();
                   String pathimmagine=document[i].data['pathimmagine'].toString();

      return new Card (
        
        color: selectProduct.contains(document[i].documentID) ? Colors.blueAccent : Colors.white,
        child:
            
             ListTile(
              
             selected: selectProduct.contains(document[i].documentID),
             enabled:  !selectProduct.contains(document[i].documentID),
              title: 
              
                  Column(
                    children: <Widget>[
                      Column(children: <Widget>[
                      Image.network(urlimmagine,width: 100,height: 100),
              ],),
              
              
              Padding(
                  padding: const EdgeInsets.only(left:1.0),
                  child: Column(        
                          children: <Widget>[
                            
                            Text(etichetta),
                            
                            Padding(
                              padding: const EdgeInsets.only(top:4.0),
                              child: Text('Data fornitura'),
                            ),
                            Text(data),
                          //  Text(controllo)
                          ],
                        ),
              ),
              
                    ],
                  ), 
           onLongPress: (){
             //implementare funzione dove settare non abilitato il documento
             //toggleSelection();
             // adddatacheck(etichetta,pathimmagine,nomefornitore,ivafornitore,quantita,note,data);
            
             //deletesingle(document[i].documentID);
             showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Disabilitare materia prima?"),
            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new FlatButton(
                  child: new Text("No"),
                  onPressed: () {
                   Navigator.pop(context);
                  },
                ),
                new FlatButton(
                  child: new Text("Si"),
                  onPressed: () {
                   
                     Firestore.instance.runTransaction((Transaction transaction)async {
                    DocumentSnapshot snapshot=
                    await transaction.get(document[i].reference);
                    await transaction.update(snapshot.reference, {
                       "abilitato": '1',


                    });
                  });

                  showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text("Rimozione effettuata"),
            ],
          ),
         
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new FlatButton(
                  child: new Text("TORNA ALLA LISTA"),
                  onPressed: () {
                   reload();
                  },
                ),
              ],
            ),
           
          ],
        );
      },
    );
                    
                   

                  },
                ),
              ],
            ),
           
          ],
        );
      },
    );


             
           
           
           },
           onTap: (){

             _onCategorySelected(true,document[i].documentID);
             adddatacheck(etichetta,pathimmagine,nomefornitore,ivafornitore,quantita,note,data,document[i].documentID);
             


             //setState(() {
              // document[i].isSelected = !document[i].isSelected;
               //document[i].documentID.contains(test);
              // });
            
            // adddatacheck(etichetta,pathimmagine,nomefornitore,ivafornitore,quantita,note,data);
             
           }, ),
        );
       
       
       
       



       
      








     }




  ),
               ),

      
      
      
      
      
      
      
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _getContent();
  }
}





