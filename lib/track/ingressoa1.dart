import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/ingressoa2.dart';


class ingressoa1 extends StatefulWidget{
  
  @override
  _ingressoa1State createState() => new _ingressoa1State(); 
}

class _ingressoa1State extends State<ingressoa1>{
 

    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Seleziona fornitore'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("fornitori")
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String nome= document[i].data['nome'].toString();
            String indirizzo= document[i].data['indirizzo'].toString();
            String iva= document[i].data['iva'].toString();
            String telefono= document[i].data['telefono'].toString();

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: new Text(
                   nome
                ,style: TextStyle(fontSize: 20 ),
                ),
                subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:8.0),
                  child: new Text("Partita iva : $iva ",style: TextStyle(fontSize: 20),),
                ),
               
                ])
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.add_box),
                        Text('Seleziona',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 

                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new ingressoa2(
                            nome: nome,
                            iva: iva,
                            
                            index: document[i].reference,
                          
                          )
                        )
                        );


                      },
                    ),
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _ingressoa1State createState() => new _ingressoa1State();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
