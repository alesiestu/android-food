import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/cronologia/prodotti/dettagli.dart';


class storyprodotti extends StatefulWidget{

  storyprodotti({this.data});
  final String data;
  
  @override
  _storyprodottiState createState() => new _storyprodottiState(); 
}

class _storyprodottiState extends State<storyprodotti>{
 
   String data;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    data=widget.data;
  }
    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Prodotti del $data'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("prodottifiniti").where("dataproduzione", isEqualTo: data)
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String nome= document[i].data['prodotto'].toString();
            String indirizzo= document[i].data['indirizzo'].toString();
            String iva= document[i].data['iva'].toString();
            String telefono= document[i].data['telefono'].toString();

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: new Text(
                   nome.toUpperCase()
                ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),textAlign: TextAlign.center,
                ),
                subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                
               
                ])
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.add_box),
                        Text('Visualizza dettagli',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 

                        Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new dettagliprodotti(
                           sessione: document[i].data['codicesessione'],
                          
                          )
                        )
                        );


                      },
                    ),
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _storyprodottiState createState() => new _storyprodottiState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
